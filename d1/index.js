console.log("Hello World");

console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");
console.log("Mayen Comia");


// Function
	// Functions in JS are lines/blocks of code that tell our device/ application to perform a certain task when called/invoked

// Function Declarations
// function statement defines a function witht he specified parameters

//Syntax
/*
	function functionName(){
		code block (statement)
	};
*/ 

// function keyword - used to define a JS function
//functionName - functions are named to be able to use later in the code
//function block ({}) - statements which comprise the body of the function
					//- this is where the code is to be executed
function printName (){
	console.log("Mayen");
}

printName();


// semicolons ; are used to separate executable JS statements

//function invocation

//call/invoke he functions that we declared
printName();

//printAge();//result in error, much like var have yet to define

//function declaration vs expression

//function declaration
function declaredFunction() {
	console.log("Hello World from declaredFunction")
};

declaredFunction();

//function expression
//a function can also be stored in a variable. this is called a function variable


//a function expression is an anonymous function assigned to the variableFunction

// anonymous - function without a name
let variableFunction = function(){
	console.log("Hello from variableFunction!");
};

variableFunction();

//a function expression of a function named funcName assigned to the variable funcExpression

//They are ALWAYS invoked (called) using variable name

let funcExpression = function funcName() {
	console.log("Hello from the other side!!!")
};

funcExpression();

//we can reassign declared functions and function expressions to new anonymous functions

declaredFunction = function() {
	console.log("updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression");
};
funcExpression();

const constantFunc = function(){
	console.log("Initialized with const!")
}

constantFunc();

// constantFunc = function(){
// 	console.log("Cannot be reassigned")
// };//error

//Function Scoping

/*
	Scoping is the accessibility of variables

	Javascript var has 3 types
		1. local/block
		2. global
		3. function scope


*/

{
	let localVar = "Juan Dela Cruz";
}

let globalVar = "Mr. Worldwide"

console.log(globalVar);
//console.log(localVar);//result in error

/*JS has a function scope:
	each function creates a new scope
	var defined inside a function are not accessible (visible) from outside the function
*/

function showNames(){
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

//console.log(functionConst);//error:not defined
//console.log(functionLet);

//Nested Functions

	//func within  a func

	function myNewFunction(){

		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name + " is used in the nestedFunction and also " + nestedName);
		}
		//console.log(nestedName);//result to an error
		//nestedName var, being declared in the nestedFunction cannot be accessed outside of the function it was declared in
		nestedFunction();
	}

	myNewFunction();//Jane
	//nestedFunction();//returns an error
	//moreover, this func is declared inside myNewFunction, it too cannot be invoked outside of the function it was declared in

//Function and Global Scope Variables

	let globalName = "Cardo";

	function myNewFunction2(){
		let nameInside = "Mario"

		console.log(globalName);
		//variables declared globally or (outside any function) hAVE a global scope 
		//global variables can be accessed from anywhere in a js program including from inside a function
	}

	myNewFunction2();//Cardo
	//console.log(nameInside);//error
		//nameInside is function-scoped. It cannot be accessed globally

//Alert
	//using alert()
	//small window to show short dialogue

	alert("Hello World");

function showSampleAlert(){
	alert("Hello User! Welcome to B248 Class!")
};

showSampleAlert();

console.log("I will only log in the console when the alert is dismissed");

//Prompt
	//using prompt()
	//small window to gather user input
	//the INPUT from the prompt will be returned as a string once the user dismisses the window

let samplePrompt = prompt("Enter your name!");

	console.log("Hello, " + samplePrompt);

let sampleNullPrompt = prompt("Don't Enter Anything!");
	console.log(sampleNullPrompt);//prompt()returns an empty string when there is no input or null if the user cancels the prompt


function printWelcomeMessage(){
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");
	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to B248 Class!");
}

printWelcomeMessage();

//Function Naming Convention
//should be definitive of task it will perform
//verb

function getCourses(){
	let courses = ["Scie","Eng","Math","Astrophy"];
	console.log(courses);
}

getCourses();

//avoid pointless names
function pikachu(){
	let name = "Optimus Prime";
	console.log(name);
}

//avoid generic
get();

function get(){
	let name= "Cardo Dalisay";
	console.log(name);

}
get();

//name your functions in small caps follow camelCase when naming variables and functions

function displayCarInfo(){
	console.log("Brand:Toyota");
	console.log("Type:Sedan");
	console.log("Price: 1,500,000");

}
displayCarInfo();