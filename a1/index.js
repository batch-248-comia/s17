/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:

function printWelcomeMessage(){
	let fullName = prompt("Enter your full name");
	let age = prompt("Enter your age");
	let address = prompt("Enter where you are from");
	console.log("Hello, " + fullName + "!");
	console.log("You are " + age + " years old.");
	console.log("You live in " + address);
	alert("Thank you for your input!");
}

printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function showFavoriteBands(){
	let band1 = "SideA";
	let band2 = "BTS";
	let band3 = "BlackPink";
	let band4 = "LANY";
	let band5 = "Hillsong United";

	console.log("1. " + band1);
	console.log("2. " + band2);
	console.log("3. " + band3);
	console.log("4. " + band4);
	console.log("5. " + band5);

}
showFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function showFaveMovieRating(){
	let mov1 = "UP";
	let mov2 = "The Notebook";
	let mov3 = "If Only";
	let mov4 = "Encanto";
	let mov5 = "Ice Age 1";

	console.log("1. " + mov1);
	console.log("Rotten Tomatoes Rating: 98%")
	console.log("2. " + mov2);
	console.log("Rotten Tomatoes Rating: 53%")
	console.log("3. " + mov3);
	console.log("Rotten Tomatoes Rating: 85%")
	console.log("4. " + mov4);
	console.log("Rotten Tomatoes Rating: 91%")
	console.log("5. " + mov5);
	console.log("Rotten Tomatoes Rating: 77%")

}

showFaveMovieRating();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

